defmodule SQSProject do
  @receive_options [max_number_of_messages: 10, visibility_timeout: 10]

  def move_messages() do
    Parallel.parallel_execute(
      Enum.to_list(1..500_000),
      fn x ->
        operation = ExAws.SQS.receive_message(dead_letter_queue(), @receive_options)

        case ExAws.request(operation, region: source_region()) do
          {:ok, %{body: %{messages: messages}, status_code: 200}} ->
            if !Enum.empty?(messages) do
              messages_batch =
                messages
                |> Enum.map(& &1.body)
                |> Enum.with_index()
                |> Enum.map(fn {message, index} -> [id: index, message_body: message] end)

              operation = ExAws.SQS.send_message_batch(queue(), messages_batch)

              case ExAws.request(operation, region: target_region()) do
                {:ok, %{status_code: 200}} ->
                  Enum.each(messages, fn %{receipt_handle: receipt_handle} ->
                    operation = ExAws.SQS.delete_message(dead_letter_queue(), receipt_handle)
                    ExAws.request!(operation, region: source_region())
                    IO.puts("Message moved: #{receipt_handle}")
                  end)

                error ->
                  # IO.puts("Mesagens não movidas, error: #{inspect(error)}")
                  nil
              end
            end
        end
      end,
      300,
      100_000_000
    )
  end

  defp queue() do
    queue_url(Application.get_env(:sqs_project, :queue), "us-east-1")
  end

  defp dead_letter_queue() do
    queue_url(Application.get_env(:sqs_project, :dead_letter_queue), "us-east-1")
  end

  defp queue_url(queue_name, region) do
    {:ok, %{body: %{queue_url: queue_url}}} =
      queue_name
      |> ExAws.SQS.get_queue_url()
      |> ExAws.request(region: region)

    queue_url
  end

  defp target_region(), do: Application.get_env(:sqs_project, :target_region)
  defp source_region(), do: Application.get_env(:sqs_project, :source_region)
end
