defmodule Parallel do
  def pmap(collection, fun, timeout \\ 480) do
    me = self()
    init_time = :os.system_time(:milli_seconds)

    collection
    |> Enum.map(fn elem ->
      {elem, spawn_monitor(fn -> send(me, {self(), fun.(elem)}) end)}
    end)
    |> Enum.map(fn {elem, {pid, ref}} ->
      receive do
        {^pid, result} ->
          result

        {:DOWN, ^ref, :process, ^pid, msg} ->
          send_error(elem, {:error, msg})
      after
        timeout_max(timeout, init_time) ->
          if Process.alive?(pid) do
            Process.exit(pid, :timeout)
          end

          :error_timeout
      end
    end)
  end

  def parallel_execute(list, fun, threads_count, timeout) do
    list
    |> Enum.chunk_every(threads_count)
    |> Enum.map(&Parallel.pmap(&1, fun, timeout))
    |> Enum.concat()
  end

  defp send_error(arg, error = {:error, msg}) do
    IO.inspect(error)
    error
  end

  defp timeout_max(timeout, init_time) do
    atual_time = :os.system_time(:milli_seconds)
    max(0, timeout - (atual_time - init_time))
  end

  defmacro __using__(_) do
    quote do
      import Parallel
    end
  end

  defmacro parallel_map_reduce(params) do
    functions =
      params
      |> Keyword.get(:pmap, [])
      |> Enum.map(fn x -> quote(do: fn -> unquote(x) end) end)

    quote do
      functions_mapping = unquote(functions)
      function_reduce = unquote(Keyword.get(params, :reduce, fn x -> x end))
      timeout = unquote(Keyword.get(params, :timeout, 450))
      Parallel.__parallel_map_reduce__(functions_mapping, function_reduce, timeout)
    end
  end

  def __parallel_map_reduce__(functions_mapping, function_reduce, timeout) do
    functions_mapping
    |> Parallel.pmap(&apply(&1, []), timeout)
    |> function_reduce.()
  end
end
